# Kalieki Food Truck CLI
by Seth Van Grinsven

### Prerequisites:

Maven installed.

### To Build:
To build the application in the root of the project run.

```mvn clean install```

### To Run:
To run the application in the root of the project run

```mvn exec:java```

### Logging:
Logging level can be configured in resources/log4j.properties.
It can also be set dynamically using System.Environment property "LogLevel"

Levels: 
- Info
- Debug
- Warn
- Error
- None

### Command Line Arguments:
(if you run via mvn, arguments are passed in via: -Dexec.args='[args]')

```-n=[number]```

Determines the page size count for the output