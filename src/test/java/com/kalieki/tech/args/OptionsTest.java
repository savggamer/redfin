package com.kalieki.tech.args;

import com.kalieki.tech.SpringBaseTestCfg;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class OptionsTest extends SpringBaseTestCfg {

    @Autowired
    private Options options;

    @Test
    public void testingNoArgs(){
        options.parse();
        Options defaultOptions = new Options();
        assertThat(options).isEqualToComparingFieldByField(defaultOptions);
    }

    @Test
    public void testingInvalidArgsReturnsDefaults(){
        options.parse("--SomeInvalidArg");
        Options defaultOptions = new Options();
        assertThat(options).isEqualToComparingFieldByField(defaultOptions);
    }

    @Test
    public void testingThatThePageSizeIsSet(){
        options.parse("-n=50");
        assertThat(options.getPageSize()).isEqualTo(50);
    }

    @Test
    public void testingPageSizeZero(){
        options.parse("-n=0");
        assertThat(options.getPageSize()).isEqualTo(10);
    }

    @Test
    public void testingPageSizeMalformed(){
        options.parse("-n=a");
        assertThat(options.getPageSize()).isEqualTo(10);
    }
}