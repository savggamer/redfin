package com.kalieki.tech.dao;

import com.kalieki.tech.dao.impl.HttpTruckDao;
import com.kalieki.tech.mappers.CustomMapperFactory;
import org.joda.time.DateTime;
import org.junit.Test;

import java.time.DayOfWeek;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class HttpTruckDaoTest extends HttpTruckDao {

    public HttpTruckDaoTest() {
        super(null);
    }

    @Test
    public void TestingMonday(){
        DateTime date = DateTime.now().withDayOfWeek(DayOfWeek.MONDAY.getValue());
        assertThat(this.getDayFriendlyString(date)).isEqualTo("Monday");
    }

    @Test
    public void TestingTuesday(){
        DateTime date = DateTime.now().withDayOfWeek(DayOfWeek.TUESDAY.getValue());
        assertThat(this.getDayFriendlyString(date)).isEqualTo("Tuesday");
    }

    @Test
    public void TestingWednesday(){
        DateTime date = DateTime.now().withDayOfWeek(DayOfWeek.WEDNESDAY.getValue());
        assertThat(this.getDayFriendlyString(date)).isEqualTo("Wednesday");
    }

    @Test
    public void TestingThursday(){
        DateTime date = DateTime.now().withDayOfWeek(DayOfWeek.THURSDAY.getValue());
        assertThat(this.getDayFriendlyString(date)).isEqualTo("Thursday");
    }

    @Test
    public void TestingFriday(){
        DateTime date = DateTime.now().withDayOfWeek(DayOfWeek.FRIDAY.getValue());
        assertThat(this.getDayFriendlyString(date)).isEqualTo("Friday");
    }

    @Test
    public void TestingSaturday(){
        DateTime date = DateTime.now().withDayOfWeek(DayOfWeek.SATURDAY.getValue());
        assertThat(this.getDayFriendlyString(date)).isEqualTo("Saturday");
    }

    @Test
    public void TestingSunday(){
        DateTime date = DateTime.now().withDayOfWeek(DayOfWeek.SUNDAY.getValue());
        assertThat(this.getDayFriendlyString(date)).isEqualTo("Sunday");
    }

    @Test
    public void Testing24HourDigit(){
        assertThat(this.format24hrStr(0)).isEqualTo("'00:00'");
        assertThat(this.format24hrStr(9)).isEqualTo("'09:00'");
        assertThat(this.format24hrStr(20)).isEqualTo("'20:00'");
    }

}