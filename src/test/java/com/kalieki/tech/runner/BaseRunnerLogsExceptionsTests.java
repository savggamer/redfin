package com.kalieki.tech.runner;

import com.kalieki.tech.runner.exception.ParseException;
import com.kalieki.tech.runner.exception.PrintException;
import com.kalieki.tech.runner.exception.RetrievalException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class BaseRunnerLogsExceptionsTests extends BaseRunner{

    private Exception expectedException;
    private Exception loggedException;

    public BaseRunnerLogsExceptionsTests() {
    }

    @Before
    public void init(){
        this.expectedException = null;
        this.loggedException = null;
    }

    @Test
    public void TestingParseLogsError() {
        this.expectedException = new ParseException();
        Throwable passthrough = catchThrowable(this::parse);
        assertThat(passthrough).isNotNull().isEqualTo(this.expectedException);
    }


    @Test
    public void TestingRetrieveException() {
        this.expectedException = new RetrievalException();
        Throwable passthrough = catchThrowable(this::retrieve);
        assertThat(passthrough).isNotNull().isEqualTo(this.expectedException);    }


    @Test
    public void TestingPrintException() {
        this.expectedException = new PrintException();
        Throwable passthrough = catchThrowable(this::print);
        assertThat(passthrough).isNotNull().isEqualTo(this.expectedException);    }

    @After
    public void AssertExceptionThrown(){
        assertThat(this.expectedException).isNotNull();
        assertThat(this.loggedException).isNotNull().isEqualTo(this.expectedException);
    }



    protected void retrieveImpl() throws RetrievalException {
        throw (RetrievalException) expectedException;
    }

    protected void parseImpl() throws ParseException {
        throw (ParseException) expectedException;
    }

    protected void printImpl() throws PrintException {
        throw (PrintException) expectedException;
    }

    @Override
    protected void LogEnd(Exception e) {
        this.loggedException = e;
        super.LogEnd(e);
    }
}
