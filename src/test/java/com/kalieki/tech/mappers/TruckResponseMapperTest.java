package com.kalieki.tech.mappers;

import com.kalieki.tech.SpringBaseTestCfg;
import com.kalieki.tech.dao.response.TruckEntryResponse;
import com.kalieki.tech.models.TruckEntry;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class TruckResponseMapperTest extends SpringBaseTestCfg {

    public static final String TEST_LOCATION = "TEST_LOCATION";
    @Autowired
    private CustomMapperFactory mapperFactory;

    private TruckEntry to;

    private TruckEntryResponse from;

    @Before
    public void init(){
        to = new TruckEntry();
        from = new TruckEntryResponse();
    }

    @Test
    public void TestingEmptyObjectsMapToEmpty(){
        map();
        assertThat(to).isNotNull();
        assertThat(to.getEnd()).isNull();
        assertThat(to.getOpen()).isNull();
        assertThat(to.getLocation()).isNull();
    }

    @Test
    public void TestingLocationMapped(){
        from.setLocation(TEST_LOCATION);
        map();
        assertThat(to.getLocation()).isEqualTo(TEST_LOCATION);
    }

    @Test
    public void TestingOpen(){
        from.setStart24("13:00");
        map();
        assertThat(to.getOpen().getHourOfDay()).isEqualTo(12);
    }

    @Test
    public void TestingClose(){
        from.setEnd24("11:00");
        map();
        assertThat(to.getEnd().getHourOfDay()).isEqualTo(10);
    }

    private void map(){
        this.mapperFactory.map(from, to);
    }
}