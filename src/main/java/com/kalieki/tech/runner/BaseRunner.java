package com.kalieki.tech.runner;

import com.kalieki.tech.runner.exception.ParseException;
import com.kalieki.tech.runner.exception.PrintException;
import com.kalieki.tech.runner.exception.RetrievalException;
import org.apache.log4j.Logger;
import org.springframework.util.StopWatch;

//Base to wrap runner with functionality like logging and timing
public abstract class BaseRunner {

    private static final String BEGIN_STATE = "Begin";
    private static final String END_STATE = "End";

    private StopWatch timer;

    private static final Logger logger = Logger.getLogger(BaseRunner.class);

    protected BaseRunner() {
    }

    public void retrieve() throws RetrievalException {
        try {
            this.startTimer();
            this.LogBegin();
            retrieveImpl();
            this.LogEnd(null);
        } catch (RetrievalException e) {
            this.LogEnd(e);
            throw e;
        }
    }

    public void parse() throws ParseException {
        try {
            this.LogBegin();
            parseImpl();
            this.LogEnd(null);
        } catch (ParseException e) {
            this.LogEnd(e);
            throw e;
        }
    }

    public void print() throws PrintException {
        try {
            this.LogBegin();
            printImpl();
            this.LogEnd(null);
        } catch (PrintException e) {
            this.LogEnd(e);
            throw e;
        }
    }

    protected abstract void retrieveImpl() throws RetrievalException;

    protected abstract void parseImpl() throws ParseException;

    protected abstract void printImpl() throws PrintException;

    private void LogBegin() {
        String methodName = this.reflectCallerMethodName();
        logger.debug("--- " + BEGIN_STATE + " " + methodName + " ---");
        this.startTimer();
    }

    protected void LogEnd(Exception e) {
        this.endTimeAndLog();
        String methodName = this.reflectCallerMethodName();
        logger.debug(e == null ? "Success" : e.getMessage(), e);
        logger.debug("--- " + END_STATE + " " + methodName + " ---");
    }


    private void startTimer() {
        this.timer = new StopWatch();
        this.timer.start();
    }

    private void endTimeAndLog() {
        this.timer.stop();
        long ms = this.timer.getTotalTimeMillis();
        logger.debug("Total Time: " + ms + "ms");
    }

    /*
   get the method name so if we refactor the method name we dont have to refactor the logging strings.
    */
    private String reflectCallerMethodName() {
        int methodDepth = 3;
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        return stackTraceElements.length > methodDepth ? stackTraceElements[methodDepth].getMethodName() : "Unknown";
    }
}
