package com.kalieki.tech.runner.exception;

public class ParseException extends Exception {
    public ParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseException() {
    }
}
