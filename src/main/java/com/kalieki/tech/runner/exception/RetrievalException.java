package com.kalieki.tech.runner.exception;

public class RetrievalException extends Exception {
    public RetrievalException(String errMessage, Exception e) {
        super(errMessage, e);
    }

    public RetrievalException() {
    }
}
