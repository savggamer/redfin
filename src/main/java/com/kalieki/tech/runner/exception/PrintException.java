package com.kalieki.tech.runner.exception;

public class PrintException extends Exception {
    public PrintException(String errMessage, Exception e) {
        super(errMessage, e);
    }

    public PrintException() {
    }
}
