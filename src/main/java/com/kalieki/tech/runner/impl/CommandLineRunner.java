package com.kalieki.tech.runner.impl;

import com.kalieki.tech.dao.ITruckDao;
import com.kalieki.tech.io.IResultsOut;
import com.kalieki.tech.models.TruckEntry;
import com.kalieki.tech.runner.BaseRunner;
import com.kalieki.tech.runner.exception.ParseException;
import com.kalieki.tech.runner.exception.PrintException;
import com.kalieki.tech.runner.exception.RetrievalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class CommandLineRunner extends BaseRunner {

    private final ITruckDao dao;

    private List<TruckEntry> results = new ArrayList<>();

    private final IResultsOut printer;

    @Autowired
    public CommandLineRunner(ITruckDao truckDao, IResultsOut printer) {
        this.dao = truckDao;
        this.printer = printer;
    }

    protected void retrieveImpl() throws RetrievalException {
        try {
            this.results = this.dao.retrieve();
        } catch (Exception e) {
            throw new RetrievalException("Retrieval Failed", e);
        }
    }

    protected void parseImpl() throws ParseException {
        try {
            this.results.sort(Comparator.comparing(TruckEntry::getName));
        } catch (Exception e) {
            throw new ParseException("Parse Failed", e);
        }
    }

    protected void printImpl() throws PrintException {
        try {
            printer.printHeaders();
            printer.printResults(this.results);
        } catch (Exception e) {
            throw new PrintException("Print Failed", e);
        }
    }
}
