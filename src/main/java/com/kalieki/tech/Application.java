package com.kalieki.tech;

import com.kalieki.tech.args.Options;
import com.kalieki.tech.runner.BaseRunner;
import com.kalieki.tech.runner.exception.ParseException;
import com.kalieki.tech.runner.exception.PrintException;
import com.kalieki.tech.runner.exception.RetrievalException;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {Application.COM_KALIEKI_TECH})
public class Application {

    public static final String COM_KALIEKI_TECH = "com.kalieki.tech";
    private static final Logger logger = Logger.getLogger(Application.class);

    public static void main(String[] args) {
        logger.debug("Creating Spring Context");
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(Application.COM_KALIEKI_TECH);

        //Setup options singleton bean
        Options options = ctx.getBean(Options.class);
        options.parse(args);

        //Create runner
        BaseRunner runner = ctx.getBean(BaseRunner.class);

        int result = 0;
        try {
            runner.retrieve();
            runner.parse();
            runner.print();
        } catch (RetrievalException e) {
            result = -1;
        } catch (ParseException e) {
            result = -2;
        } catch (PrintException e) {
            result = -3;
        }

        System.exit(result);
    }
}
