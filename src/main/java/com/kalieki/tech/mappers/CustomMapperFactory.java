package com.kalieki.tech.mappers;

import com.kalieki.tech.dao.response.TruckEntryResponse;
import com.kalieki.tech.models.TruckEntry;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomMapperFactory extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(TruckEntryResponse.class, TruckEntry.class)
                .customize(new TruckResponseMapper())
                .register();
    }

}
