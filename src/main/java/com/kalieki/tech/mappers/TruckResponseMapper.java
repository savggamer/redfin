package com.kalieki.tech.mappers;

import com.kalieki.tech.dao.response.TruckEntryResponse;
import com.kalieki.tech.models.TruckEntry;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.joda.time.LocalTime;

public class TruckResponseMapper extends CustomMapper<TruckEntryResponse, TruckEntry> {

    @Override
    public void mapAtoB(TruckEntryResponse truckEntryResponse, TruckEntry truckEntry, MappingContext context) {
        truckEntry.setLocation(truckEntryResponse.getLocation());
        truckEntry.setName(truckEntryResponse.getApplicant());

        if (truckEntryResponse.getStart24() != null) {
            truckEntry.setOpen(this.parseTime(truckEntryResponse.getStart24()));
        }

        if (truckEntryResponse.getEnd24() != null) {
            truckEntry.setEnd(this.parseTime(truckEntryResponse.getEnd24()));
        }

        truckEntry.setDay(truckEntryResponse.getDayofweekstr());
        truckEntry.setOpenStr(truckEntryResponse.getStarttime());
        truckEntry.setEndStr(truckEntryResponse.getEndtime());
    }

    private LocalTime parseTime(String end24) {
        String[] parts = end24.split(":");
        //Convert from server side 1-24 -> 0 - 23
        return new LocalTime(Integer.parseInt(parts[0]) - 1, Integer.parseInt(parts[1]));
    }
}
