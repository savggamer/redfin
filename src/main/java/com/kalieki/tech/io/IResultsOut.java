package com.kalieki.tech.io;

import com.kalieki.tech.models.TruckEntry;

import java.io.IOException;
import java.util.List;

public interface IResultsOut {

    void printHeaders();

    void printResults(List<TruckEntry> results) throws IOException;
}
