package com.kalieki.tech.io.impl;

import com.kalieki.tech.args.Options;
import com.kalieki.tech.io.IResultsOut;
import com.kalieki.tech.models.TruckEntry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

@Component
public class ConsolePagingResultsOut implements IResultsOut {

    private static final String COUNT_HEADER = "Count";
    private final Options options;

    @Autowired
    public ConsolePagingResultsOut(Options options) {
        this.options = options;
    }

    private static final String TABLE_FORMAT_STRING = " %-5s| %-75s| %-75s| %-10s| %-10s| %-11s|";
    private static final Logger logger = Logger.getLogger(ConsolePagingResultsOut.class);

    private static final String NAME_HEADER = "Name";
    private static final String LOCATION_HEADER = "Location";
    private static final String DATE_HEADER = "Date";
    private static final String OPEN_HEADER = "Open Time";
    private static final String CLOSE_HEADER = "Close Time";

    @Override
    public void printHeaders() {
        String header = String.format(TABLE_FORMAT_STRING, COUNT_HEADER, NAME_HEADER, LOCATION_HEADER, DATE_HEADER, OPEN_HEADER, CLOSE_HEADER);
        logger.info(header);
    }

    @Override
    public void printResults(List<TruckEntry> results) throws IOException {
        int pageSize = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        for (int a = 0; a < results.size(); a++) {
            TruckEntry rowObj = results.get(a);
            String row = String.format(TABLE_FORMAT_STRING, a, rowObj.getName(), rowObj.getLocation(), rowObj.getDay(), rowObj.getOpenStr(), rowObj.getEndStr());
            logger.info(row);
            pageSize++;

            if (pageSize == this.options.getPageSize()) {
                br.read();
                pageSize = 0;
            }
        }
    }
}
