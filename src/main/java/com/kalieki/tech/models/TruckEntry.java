package com.kalieki.tech.models;

import org.joda.time.LocalTime;

public class TruckEntry {

    private String name;
    private String location;
    private LocalTime open;
    private LocalTime end;
    private String day;
    private String openStr;
    private String endStr;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalTime getOpen() {
        return open;
    }

    public void setOpen(LocalTime open) {
        this.open = open;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getOpenStr() {
        return openStr;
    }

    public void setOpenStr(String openStr) {
        this.openStr = openStr;
    }

    public String getEndStr() {
        return endStr;
    }

    public void setEndStr(String endStr) {
        this.endStr = endStr;
    }
}
