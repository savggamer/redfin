package com.kalieki.tech.dao.impl;

import com.kalieki.tech.dao.ITruckDao;
import com.kalieki.tech.dao.response.TruckEntryResponseList;
import com.kalieki.tech.mappers.CustomMapperFactory;
import com.kalieki.tech.models.TruckEntry;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;

@Component
public class HttpTruckDao implements ITruckDao {

    private final CustomMapperFactory customMapperFactory;

    private static final String ENDPOINT = "https://data.sfgov.org/resource/bbb8-hzi6.json";

    private final Logger logger = Logger.getLogger(HttpTruckDao.class);

    @Autowired
    public HttpTruckDao(CustomMapperFactory customMapperFactory) {
        this.customMapperFactory = customMapperFactory;
    }

    public List<TruckEntry> retrieve() {
        DateTime now = DateTime.now();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new HttpTruckDaoErrorHandler());
        String uriString = buildUriString(now);
        logger.debug("Http Request: " + uriString);
        TruckEntryResponseList truckEntryResponseList = restTemplate.getForObject(uriString, TruckEntryResponseList.class);
        logger.debug("Retrieved " + (truckEntryResponseList != null ? truckEntryResponseList.  size() : "null") + " results");
        StopWatch watch = new StopWatch();
        watch.start();
        List<TruckEntry> results = customMapperFactory.mapAsList(truckEntryResponseList, TruckEntry.class);
        watch.stop();
        logger.debug("Mapped (" + watch.getTotalTimeMillis() + "ms): " + results.size());
        return results;
    }

    private String buildUriString(DateTime reqTime) {
        String curTime = this.format24hrStr(reqTime.getHourOfDay());
        String formattedDay = getDayFriendlyString(reqTime);
        String queryParams = MessageFormat.format("$where=dayofweekstr = ''{0}'' AND start24 < {1} AND end24 > {1}&$select=location, dayofweekstr, applicant, starttime, endtime", formattedDay, curTime);
        return ENDPOINT + "?" + queryParams;
    }

    protected String format24hrStr(int hourOfDay) {
        String str = "'";

        if (hourOfDay < 10) {
            str += "0";
        }

        return str + hourOfDay + ":00'";
    }

    protected String getDayFriendlyString(DateTime now) {
        int day = now.getDayOfWeek();
        return DayOfWeek.of(day).getDisplayName(TextStyle.FULL, Locale.ENGLISH);
    }

}
