package com.kalieki.tech.dao;

import com.kalieki.tech.models.TruckEntry;

import java.util.List;

public interface ITruckDao {

    List<TruckEntry> retrieve();
}
