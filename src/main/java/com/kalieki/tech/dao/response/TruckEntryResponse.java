package com.kalieki.tech.dao.response;

public class TruckEntryResponse {


    private String addr_date_create;
    private String addr_date_modified;
    private String applicant;
    private String block;
    private String cnn;
    private String coldtruck;
    private String dayofweekstr;
    private String dayorder;
    private String end24;
    private String endtime;
    private String latitude;
    private String location;
    private String location_2;
    private String locationdesc;
    private String locationid;
    private String longitude;
    private String lot;
    private String optionaltext;
    private String permit;
    private String start24;
    private String starttime;
    private String x;
    private String y;

    //        "type": "Point",
    //                "coordinates": [
    //        -122.378155718149,
    //                37.737071230506
    //      ]
    //    },


    public String getAddr_date_create() {
        return addr_date_create;
    }

    public void setAddr_date_create(String addr_date_create) {
        this.addr_date_create = addr_date_create;
    }

    public String getAddr_date_modified() {
        return addr_date_modified;
    }

    public void setAddr_date_modified(String addr_date_modified) {
        this.addr_date_modified = addr_date_modified;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getCnn() {
        return cnn;
    }

    public void setCnn(String cnn) {
        this.cnn = cnn;
    }

    public String getColdtruck() {
        return coldtruck;
    }

    public void setColdtruck(String coldtruck) {
        this.coldtruck = coldtruck;
    }

    public String getDayofweekstr() {
        return dayofweekstr;
    }

    public void setDayofweekstr(String dayofweekstr) {
        this.dayofweekstr = dayofweekstr;
    }

    public String getDayorder() {
        return dayorder;
    }

    public void setDayorder(String dayorder) {
        this.dayorder = dayorder;
    }

    public String getEnd24() {
        return end24;
    }

    public void setEnd24(String end24) {
        this.end24 = end24;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation_2() {
        return location_2;
    }

    public void setLocation_2(String location_2) {
        this.location_2 = location_2;
    }

    public String getLocationdesc() {
        return locationdesc;
    }

    public void setLocationdesc(String locationdesc) {
        this.locationdesc = locationdesc;
    }

    public String getLocationid() {
        return locationid;
    }

    public void setLocationid(String locationid) {
        this.locationid = locationid;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getOptionaltext() {
        return optionaltext;
    }

    public void setOptionaltext(String optionaltext) {
        this.optionaltext = optionaltext;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getStart24() {
        return start24;
    }

    public void setStart24(String start24) {
        this.start24 = start24;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }
}
