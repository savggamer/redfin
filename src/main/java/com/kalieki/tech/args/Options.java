package com.kalieki.tech.args;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class Options {

    private static final String PAGE_SIZE_OPTION_KEY = "n";
    private static final Logger logger = Logger.getLogger(Options.class);

    private int pageSize = 10;

    /*
    Parse the command line options into a container class to make passing to components easier.
    Currently no cmd line options exist so return empty object.
     */
    public void parse(String... commandLineArgs) {

        String level = System.getProperty("LogLevel");
        if (level != null && !level.equals("")) {
            Logger.getRootLogger().setLevel(Level.toLevel(level));
        }

        try {
            logger.debug("Beginning parsing command line options");
            CommandLineParser parser = new DefaultParser();

            org.apache.commons.cli.Options cliOptions = new org.apache.commons.cli.Options();
            cliOptions.addOption(PAGE_SIZE_OPTION_KEY, true, "Number of entries in paging");
            CommandLine line = parser.parse(cliOptions, commandLineArgs);

            parsePageCount(line);

            logger.debug("End parsing command line options");
        } catch (Exception ex) {
            logger.warn("Failed parsing command line options: " + ex.getMessage());
        }
    }

    private void parsePageCount(CommandLine cliOptions) {
        if (cliOptions.hasOption(PAGE_SIZE_OPTION_KEY)) {
            String val = cliOptions.getOptionValue(PAGE_SIZE_OPTION_KEY);
            if (val != null && !val.equals("")) {
                int parsed = Integer.parseInt(val);

                if (parsed > 0) {
                    this.pageSize = parsed;
                }
            }
            logger.debug("Setting Page Size: " + this.pageSize);
        } else {
            logger.debug("Defaulting to default page size: " + this.pageSize);
        }
    }

    public int getPageSize() {
        return pageSize;
    }
}
